;***************************************
;	continue.asm
;	Date: 22 Mar 2017
;	Homework: 
;		repeatedly ask for a and show it, until user wants to quit
;	Author: Jose Madureira
;***************************************

Include Irvine32.inc
.data
	msgGetNumber BYTE " Please enter an integer: ",0
	msgShowNumber BYTE 09h,"...you entered: ",0
	msgRpt BYTE " Do you want to reenter a number? (enter y or Y) :",0

.code
Main PROC

rpt:
	;ask for number
	call Crlf
	call Crlf
	mov edx, offset msgGetNumber
	call WriteString
	call ReadInt
	
	;show number
	mov edx, offset msgShowNumber
	call WriteString
	call WriteInt

	;ask if they want to repeat
	call Crlf
	call Crlf
	mov edx, offset msgRpt
	call WriteString
	call ReadChar		;char in AL

	;if y or Y repeat, else exit program
	cmp al,79h
	je rpt
	cmp al,59h
	je rpt

	call Crlf
	call Crlf

	exit
Main ENDP
END Main
